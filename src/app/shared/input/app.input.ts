import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
@Component({
  selector: 'app-input',
  templateUrl: './app.input.html',
  styleUrls: ['./app.input.scss'],
})
export class AppInput {
  @Output() valueChanged = new EventEmitter<string>();
  @Input() defaultValue: string | undefined;
  @Input() type?: 'text' | 'number' | 'search' = 'search';
  @Input() placeHolder?: string;
  @Input() cssClass?: string;

  inputFormGroup!: UntypedFormGroup;

  constructor() {}

  ngOnInit(): void {
    this.inputFormGroup = new UntypedFormGroup({
      input: new UntypedFormControl(this.defaultValue),
    });
  }

  sendSearchQuery(): void {
    this.valueChanged.emit(
      this.inputFormGroup.controls['input'].value as string
    );
  }

  clearSearch(): void {
    this.inputFormGroup.patchValue({ input: '' });
    this.sendSearchQuery();
  }
}
