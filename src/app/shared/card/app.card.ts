import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './app.card.html',
  styleUrls: ['./app.card.scss'],
})
export class AppCard {
  @Input() title = '';
  @Input() description?: string;
}
