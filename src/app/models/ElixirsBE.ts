export enum ElixirsDifficulty {
  Unknown = 'Unknown',
  Advanced = 'Advanced',
  Moderate = 'Moderate',
  Beginner = 'Beginner',
  OrdinaryWizardingLevel = 'OrdinaryWizardingLevel',
  OneOfAKind = 'OneOfAKind',
}
export interface shortInfo {
  id: string;
  name: string;
}

export interface inventorInfo {
  id: string;
  firstName: string;
  lastName: string;
}

export interface ElixirsBE {
  id: string;
  name: string;
  effect: string;
  sideEffects: string;
  characteristics: string;
  time: string;
  difficulty: ElixirsDifficulty;
  ingredients: shortInfo[];
  inventors: inventorInfo[];
  manufactor: string;
}
