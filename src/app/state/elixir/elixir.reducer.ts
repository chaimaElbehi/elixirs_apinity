import { createReducer, on } from '@ngrx/store';
import { loadElixir, loadElixirFail, loadElixirSucces } from './elixir.actions';
import { initialElixir } from './elixir.state';

const _elixirReducer = createReducer(
  initialElixir,
  on(loadElixir, (state) => {
    return {
      ...state,
    };
  }),
  on(loadElixirSucces, (state, action) => {
    return {
      ...state,
      elixir: action.elixir,
      error: ' ',
    };
  }),
  on(loadElixirFail, (state, action) => {
    return {
      ...state,
      elixir: null,
      Errormessage: action.errorMessage,
    };
  })
);
export function elixirReducer(state: any, action: any) {
  return _elixirReducer(state, action);
}
