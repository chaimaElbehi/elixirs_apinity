import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ElixirState } from './elixir.state';

const getElixrstate = createFeatureSelector<ElixirState>('elixir');

export const getElixirInfo = createSelector(getElixrstate, (state) => {
  return state.elixir;
});
