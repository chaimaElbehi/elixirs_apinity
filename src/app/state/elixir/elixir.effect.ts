import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ElixirsService } from 'src/app/services/elixirs.service';
import {
  LOAD_ELIXIR,
  loadElixirFail,
  loadElixirSucces,
} from './elixir.actions';
import { catchError, debounceTime, exhaustMap, map, of } from 'rxjs';
import { ElixirsBE } from 'src/app/models/ElixirsBE';
@Injectable()
export class ElixirEffects {
  constructor(
    private action$: Actions,
    private elixirService: ElixirsService
  ) {}

  _elixir = createEffect(() =>
    this.action$.pipe(
      debounceTime(100),
      ofType(LOAD_ELIXIR),
      exhaustMap((action: { id: string }) => {
        return this.elixirService.getElixirById(action.id).pipe(
          map((elixir: ElixirsBE) => {
            return loadElixirSucces({ elixir });
          }),
          catchError((_error) => of(loadElixirFail({ errorMessage: _error })))
        );
      })
    )
  );
}
