import { createAction, props } from '@ngrx/store';
import { ElixirsBE } from 'src/app/models/ElixirsBE';

export const LOAD_ELIXIR = '[elixir details page] load elixir';
export const LOAD_ELIXIR_SUCESS = '[elixir details page] load elixir success';
export const LOAD_ELIXIR_FAIL = '[elixir details page] load elixir failed!';

export const loadElixir = createAction(LOAD_ELIXIR, props<{ id: string }>());
export const loadElixirFail = createAction(
  LOAD_ELIXIR_FAIL,
  props<{ errorMessage: string }>()
);

export const loadElixirSucces = createAction(
  LOAD_ELIXIR_SUCESS,
  props<{ elixir: ElixirsBE }>()
);
