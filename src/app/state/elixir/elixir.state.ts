import { ElixirsBE } from 'src/app/models/ElixirsBE';

export interface ElixirState {
  elixir: ElixirsBE | null;
  error: string | null;
  status: 'loading' | 'error' | 'success' | 'pending';
}
export const initialElixir: ElixirState = {
  elixir: null,
  error: null,
  status: 'pending',
};
