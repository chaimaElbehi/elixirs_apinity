import { elixirReducer } from '../elixir/elixir.reducer';
import { elixirsReducer } from '../elixirs/elixirs.reducer';

export const AppState = {
  elixir: elixirReducer,
  elixirs: elixirsReducer,
};
