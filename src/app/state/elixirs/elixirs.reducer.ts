import { createReducer, on } from '@ngrx/store';
import {
  loadElixirs,
  loadElixirsFail,
  loadElixirsSucces,
} from './elixirs.actions';
import { initialElixirs } from './elixirs.state';

const _elixirsReducer = createReducer(
  initialElixirs,
  on(loadElixirs, (state) => {
    return {
      ...state,
    };
  }),
  on(loadElixirsSucces, (state, action) => {
    return {
      ...state,
      elixirsList: [...action.elixirsList],
      error: ' ',
    };
  }),
  on(loadElixirsFail, (state, action) => {
    console.log(action.errorMessage);
    return {
      ...state,
      elixirsList: [],
      Errormessage: action.errorMessage,
    };
  })
);
export function elixirsReducer(state: any, action: any) {
  return _elixirsReducer(state, action);
}
