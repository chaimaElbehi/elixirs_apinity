import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ElixirsService } from 'src/app/services/elixirs.service';
import {
  LOAD_ELIXIRS,
  loadElixirsFail,
  loadElixirsSucces,
} from './elixirs.actions';
import { catchError, debounceTime, exhaustMap, map, of } from 'rxjs';
@Injectable()
export class ElixirsEffects {
  constructor(
    private action$: Actions,
    private elixirService: ElixirsService
  ) {}

  _elixirs = createEffect(() =>
    this.action$.pipe(
      debounceTime(100),
      ofType(LOAD_ELIXIRS),
      exhaustMap((action: { name: string }) => {
        const query = { name: action.name };
        return this.elixirService.getAllElixirs(query).pipe(
          map((elixirs) => {
            return loadElixirsSucces({ elixirsList: elixirs });
          }),
          catchError((_error) => of(loadElixirsFail({ errorMessage: _error })))
        );
      })
    )
  );
}
