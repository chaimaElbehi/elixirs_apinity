import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ElixirsState } from './elixirs.state';

const getElixrsstate = createFeatureSelector<ElixirsState>('elixirs');

export const getElixirsInfo = createSelector(getElixrsstate, (state) => {
  return state.elixirsList;
});
