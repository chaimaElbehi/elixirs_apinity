import { createAction, props } from '@ngrx/store';
import { ElixirsBE } from 'src/app/models/ElixirsBE';

export const LOAD_ELIXIRS = '[elixirs page] load elixirs';
export const LOAD_ELIXIRS_SUCESS = '[elixirs page] load elixirs success';
export const LOAD_ELIXIRS_FAIL = '[elixirs page] load elixirs failed!';

export const loadElixirs = createAction(
  LOAD_ELIXIRS,
  props<{ name: string }>()
);
export const loadElixirsFail = createAction(
  LOAD_ELIXIRS_FAIL,
  props<{ errorMessage: string }>()
);

export const loadElixirsSucces = createAction(
  LOAD_ELIXIRS_SUCESS,
  props<{ elixirsList: ElixirsBE[] }>()
);
