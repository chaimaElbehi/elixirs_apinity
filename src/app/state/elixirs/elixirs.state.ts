import { ElixirsBE } from 'src/app/models/ElixirsBE';

export interface ElixirsState {
  elixirsList: ElixirsBE[];
  error: string | null;
  status: 'loading' | 'error' | 'success' | 'pending';
}
export const initialElixirs: ElixirsState = {
  elixirsList: [],
  error: null,
  status: 'pending',
};
