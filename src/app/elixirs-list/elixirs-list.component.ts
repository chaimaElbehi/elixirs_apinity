import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { loadElixirs } from '../state/elixirs/elixirs.actions';
import { getElixirsInfo } from '../state/elixirs/elixirs.selectors';
import { Observable, Subscription } from 'rxjs';
import { ElixirsBE } from '../models/ElixirsBE';

@Component({
  selector: 'elixirs-list',
  templateUrl: './elixirs-list.component.html',
  styleUrls: ['./elixirs-list.component.scss'],
})
export class ElixirsListComponent implements OnInit {
  elixirsList: Observable<ElixirsBE[] | null> = new Observable<null>();
  search = '';

  constructor(private store: Store) {}
  ngOnInit(): void {
    this.loadSingleElixir(this.search);
  }

  loadSingleElixir(id: string) {
    this.store.dispatch(loadElixirs({ name: this.search }));
    this.elixirsList = this.store.select(getElixirsInfo);
  }

  searchByName(value: string) {
    // '0106fb32-b00d-4d70-9841-4b7c2d2cca71'
    this.search = value;
    this.loadSingleElixir(this.search);
  }
}
