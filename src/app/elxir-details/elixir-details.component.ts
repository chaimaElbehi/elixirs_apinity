import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { loadElixir } from '../state/elixir/elixir.actions';
import { getElixirInfo } from '../state/elixir/elixir.selectors';
import { ElixirsBE } from '../models/ElixirsBE';

@Component({
  selector: 'elixir-details',
  templateUrl: './elixir-details.component.html',
  styleUrls: ['./elixir-details.component.scss'],
})
export class ElixirDetailsComponent implements OnInit {
  search = '';
  currentElixirId = '';
  currentElixir: Observable<ElixirsBE | null> = new Observable<null>();
  private routeSub: Subscription;
  constructor(private route: ActivatedRoute, private store: Store) {
    this.routeSub = this.route.params.subscribe((params) => {
      if (params) {
        this.currentElixirId = params['id'] as string;
      }
      this.fetchElixirDetails();
    });
  }

  fetchElixirDetails() {
    this.store.dispatch(loadElixir({ id: this.currentElixirId }));
    this.currentElixir = this.store.select(getElixirInfo);
  }
  ngOnInit(): void {}
}
