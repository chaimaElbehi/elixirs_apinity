import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ElixirsListComponent } from './elixirs-list/elixirs-list.component';
import { ElixirDetailsComponent } from './elxir-details/elixir-details.component';

const routes: Routes = [
  { path: '', redirectTo: '/landing', pathMatch: 'full' },
  { path: 'landing', component: ElixirsListComponent },
  { path: 'details/:id', component: ElixirDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), HttpClientModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
