import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ElixirsBE, ElixirsDifficulty } from '../models/ElixirsBE';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ElixirsService {
  BASE_URL = 'https://wizard-world-api.herokuapp.com/Elixirs';
  constructor(private http: HttpClient) {}

  getAllElixirs(query?: {
    name?: string;
    difficulty?: ElixirsDifficulty;
    ingridient?: string;
    inventorFullName?: string;
    manufacturer?: string;
  }): Observable<ElixirsBE[]> {
    let queryString = this.BASE_URL;
    if (query !== undefined) {
      queryString += '?';
      if (query.name !== undefined) {
        queryString += `Name=${query.name}`;
      }
      if (query.difficulty !== undefined) {
        queryString += `/Difficulty=${query.difficulty}`;
      }
      if (query.ingridient !== undefined) {
        queryString += `/Ingredient=${query.ingridient}`;
      }
      if (query.inventorFullName !== undefined) {
        queryString += `/InventorFullName=${query.inventorFullName}`;
      }
      if (query.manufacturer !== undefined) {
        queryString += `/Manufacturer=${query.manufacturer}`;
      }
    }

    return this.http.get<ElixirsBE[]>(queryString);
  }

  getElixirById(id: string): Observable<ElixirsBE> {
    return this.http.get<ElixirsBE>(`${this.BASE_URL}/${id}`);
  }
}
