import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ElixirsListComponent } from './elixirs-list/elixirs-list.component';
import { ElixirDetailsComponent } from './elxir-details/elixir-details.component';

import { SharedModules } from './shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ElixirsEffects } from './state/elixirs/elixirs.effect';
import { ElixirEffects } from './state/elixir/elixir.effect';
import { MatChipsModule } from '@angular/material/chips';

import { AppState } from './state/global/app.state';

@NgModule({
  declarations: [AppComponent, ElixirsListComponent, ElixirDetailsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModules,
    MatChipsModule,

    StoreModule.forRoot(AppState),
    EffectsModule.forRoot([ElixirsEffects, ElixirEffects]),
  ],
  exports: [MatChipsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
